from twisted.trial import unittest
from test.score_notifier_tests import TestScoreNotifier


class TestAll(unittest.TestCase):
    def test_all(self):
        testSuite = unittest.TestSuite()
        testSuite.run(TestScoreNotifier)
