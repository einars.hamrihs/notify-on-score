from twisted.trial import unittest

import src.services.score_notifier
from src.services.score_notifier import ScoreNotifier
from src.services.score_notifier import InvalidScoreThreshold
from twisted.internet import defer
from parameterized import parameterized
import mock


browser_mock = mock.MagicMock()
mailer_mock = mock.MagicMock()
settings_mock = mock.MagicMock(receiver_email="test@example.com")
request_mock = mock.MagicMock(return_value=defer.succeed(mock.MagicMock()))
read_body_mock = mock.MagicMock(return_value=defer.succeed(b"{}"))


def fake_setup_browser(cls):
    cls.browser = browser_mock


class TestScoreNotifier(unittest.TestCase):
    def setUp(self) -> None:
        self.lp_mock = mock.patch("src.services.score_notifier.task.LoopingCall")
        self.lp_mock.start()

        self.browser_patcher = mock.patch(
            "src.scrapers.sofascore.Sofascore.setup_browser", fake_setup_browser
        )
        self.browser_patcher.start()

        self.mailer_patcher = mock.patch(
            "src.services.score_notifier.Mailer.send_mail", mailer_mock
        )
        self.mailer_patcher.start()

        self.request_patcher = mock.patch(
            "src.utils.tg_notifier.Agent.request", request_mock
        )
        self.request_patcher.start()

        self.read_body_patcher = mock.patch(
            "src.utils.tg_notifier.readBody", read_body_mock
        )
        self.read_body_patcher.start()

        self.settings_patcher = mock.patch.multiple(
            src.services.score_notifier.settings,
            receiver_email="test@example.com",
            score_threshold="3:0",
        )
        self.settings_patcher.start()

        self.score_notifier = ScoreNotifier()

    def tearDown(self) -> None:
        self.lp_mock.stop()
        self.browser_patcher.stop()
        self.mailer_patcher.stop()
        self.settings_patcher.stop()
        self.request_patcher.stop()
        self.read_body_patcher.stop()

    def test_check_scores(self):
        with open("../test/test_data/test_content.html", "r") as file:
            html = file.read()

        browser_mock.page_source = html
        mailer_mock.send_mail.return_value = mock.MagicMock()
        result = self.score_notifier.check_scores()
        self.assertIsNone(result)
        mailer_mock.assert_called_with(
            "test@example.com",
            b"Subject: Game has reached 3:0 score\n\nHapoel Herzliya  - 3 : 0 - Hapoel Marmorek \n",
        )

    @parameterized.expand(
        [
            ["30"],
            ["test:test"],
        ]
    )
    def test_check_scores_invalid_score_threshold(self, score_threshold):
        with mock.patch(
            "src.services.score_notifier.settings.score_threshold", score_threshold
        ):
            with self.assertRaises(InvalidScoreThreshold):
                ScoreNotifier()
