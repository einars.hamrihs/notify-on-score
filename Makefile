PIP:="venv/bin/pip"
VENV_PYTHON:="venv/bin/python3"
SERVICE_NAME:="notify-on-score"
SERVICE_FILE:="systemd/${SERVICE_NAME}.service"

define create-venv
virtualenv venv -p python3
endef

hello:
	@echo "Hello"

create-venv:
	@${create-venv}
	@${PIP} install -r requirements.txt

setup:
	cp -u ${SERVICE_FILE} "/etc/systemd/system/"
	systemctl daemon-reload
	systemctl start ${SERVICE_NAME}
	systemctl enable ${SERVICE_NAME}

restart:
	systemctl restart ${SERVICE_NAME}

trial:
	${VENV_PYTHON} -m twisted.trial test/all_tests.py
