from twisted.logger import Logger
from twisted.internet import defer
from src.services.score_notifier import ScoreNotifier
from src.settings import Settings

settings = Settings()


class Component:

    log = Logger()

    def __init__(self):
        self.score_notifier = None
        self.register_services()

    def register_services(self):
        self.log.info("registering services..")
        self.score_notifier = ScoreNotifier()


async def main(reactor):

    Component()

    # we don't *have* to hand over control of the reactor to
    # component.run -- if we don't want to, we call .start()
    # The Deferred it returns fires when the component is "completed"
    # (or errbacks on any problems).
    # log = Logger()

    # If the Component raises an exception we want to exit. Note that
    # things like failing to connect will be swallowed by the
    # re-connection mechanisms already so won't reach here.

    # wait forever (unless the Component raises an error)

    done = defer.Deferred()

    await done
