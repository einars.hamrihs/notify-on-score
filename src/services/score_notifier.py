from twisted.internet import task
from src.scrapers.sofascore import Sofascore
from src.utils.mailer import Mailer
from src.utils.tg_notifier import TG
from src.settings import Settings
from twisted.logger import Logger

settings = Settings()


class InvalidScoreThreshold(Exception):
    pass


class ScoreNotifier:
    log = Logger()

    def __init__(self):
        self.scraper = None
        self.mailer = Mailer()
        self.tg = TG()
        self.score = settings.score_threshold
        self.first_score = None
        self.second_score = None
        self.parse_score(self.score)
        self.start()

    def parse_score(self, score):
        try:
            self.first_score = int(score.split(":")[0])
            self.second_score = int(score.split(":")[1])
        except ValueError:
            raise InvalidScoreThreshold("Both score side should be a valid integers.")
        except IndexError:
            raise InvalidScoreThreshold("Scores should be seperated with ':'.")

    def start(self) -> None:
        self.scraper = Sofascore()
        lc = task.LoopingCall(self.check_scores)
        lc.start(10)
        self.log.info(f"Score notifier service started. Waiting for score {self.score}")

    def check_scores(self) -> None:
        scores = self.scraper.get_scores()
        for score in scores:
            if (
                score["home_score"] == self.first_score
                and score["away_score"] == self.second_score
                or score["home_score"] == self.second_score
                and score["away_score"] == self.first_score
            ):
                self.log.info(f"{self.score} score found")
                content = f"""Subject: Game has reached {self.score} score\n\n{score["home_team"]} - {score["home_score"]} : {score["away_score"]} - {score["away_team"]}\n"""
                self.mailer.send_mail(settings.receiver_email, content.encode("utf-8"))
                self.tg.send_message(content)
