from bs4 import BeautifulSoup
from selenium import webdriver
from twisted.logger import Logger

from src.settings import Settings
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
import re

settings = Settings()


class Sofascore:
    log = Logger()

    def __init__(self):
        self.url = settings.sofascore_url
        self.browser = None
        self.setup_browser()

    def setup_browser(self) -> None:
        service = Service(executable_path="/usr/bin/chromedriver")
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        self.browser = webdriver.Chrome(service=service, chrome_options=chrome_options)

    def parse_scores(self, html_content: str) -> list:
        soup = BeautifulSoup(html_content, "html.parser")
        scores_block = soup.find(
            "div", class_="ReactVirtualized__Grid__innerScrollContainer"
        )

        scores_list = []
        for div in scores_block:
            teams_div = div.find("div", title=re.compile("live score"))
            if teams_div:
                teams_list = teams_div.findAll("div")
                try:
                    home_score = div.find(
                        "div", {"data-change-key": "homeScore.display"}
                    ).decode_contents()
                    away_score = div.find(
                        "div", {"data-change-key": "awayScore.display"}
                    ).decode_contents()
                    scores_list.append(
                        {
                            "home_team": teams_list[0].decode_contents(),
                            "away_team": teams_list[1].decode_contents(),
                            "home_score": int(home_score),
                            "away_score": int(away_score),
                        }
                    )
                except ValueError:
                    self.log.error("Score value is not an integer. Skipping...")

        return scores_list

    def get_scores(self) -> list:
        self.browser.get(self.url)
        html_content = self.browser.page_source
        return self.parse_scores(html_content)

    def __del__(self) -> None:
        self.browser.quit()
