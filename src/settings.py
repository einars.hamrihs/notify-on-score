from dotenv import load_dotenv
from pydantic import BaseSettings, Field


load_dotenv(verbose=True)


class Settings(BaseSettings):
    # default values to None for unit tests
    component_name: str = "notify-on-score"
    sofascore_url: str = Field(None, env="sofascore_url")
    smtp_server: str = Field(None, env="SMTP_SERVER")
    smtp_port: int = Field(None, env="SMTP_PORT")
    smtp_username: str = Field(None, env="SMTP_USERNAME")
    smtp_sender: str = Field(None, env="SMTP_SENDER")
    smtp_password: str = Field(None, env="SMTP_PASSWORD")
    receiver_email: str = Field(None, env="RECEIVER_EMAIL")
    score_threshold: str = Field(None, env="SCORE_THRESHOLD")
    tg_chat_id: str = Field(None, env="TG_CHAT_ID")
    tg_token: str = Field(None, env="TG_TOKEN")
