from twisted.internet import reactor
from twisted.web.client import Agent, readBody
from src.settings import Settings
import urllib.parse

settings = Settings()


class TG:
    def __init__(self):
        self.agent = Agent(reactor)

    """
    Can be used for getting chat ID
    """

    def get_updates(self) -> bytes:
        def on_body(response_body):
            return response_body

        def on_result(response):
            d = readBody(response)
            d.addCallback(on_body)
            return d

        d = self.agent.request(
            b"GET",
            bytes(
                f"https://api.telegram.org/bot{settings.tg_token}/getUpdates", "utf-8"
            ),
            None,
        )
        d.addCallback(on_result)
        return d

    def send_message(self, message: str) -> bytes:
        def on_body(response_body):
            return response_body

        def on_result(response):
            d = readBody(response)
            d.addCallback(on_body)
            return d

        d = self.agent.request(
            b"GET",
            bytes(
                f"https://api.telegram.org/bot{settings.tg_token}/sendMessage?chat_id={settings.tg_chat_id}&text={urllib.parse.quote(message)}",
                "utf-8",
            ),
            None,
        )
        d.addCallback(on_result)
        return d
