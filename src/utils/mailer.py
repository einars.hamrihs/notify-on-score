import smtplib
import ssl
from src.settings import Settings
from twisted.logger import Logger

settings = Settings()


class Mailer:
    log = Logger()

    def __init__(self):
        self.smtp_server = settings.smtp_server
        self.port = settings.smtp_port
        self.smtp_username = settings.smtp_username
        self.sender_email = settings.smtp_sender
        self.password = settings.smtp_password
        # Create a secure SSL context
        self.context = ssl.create_default_context()

    def send_mail(self, receiver_email: str, message: bytes) -> None:
        server = smtplib.SMTP(self.smtp_server, self.port)
        # Try to log in to server and send email
        try:
            server.ehlo()  # Can be omitted
            server.starttls(context=self.context)  # Secure the connection
            server.ehlo()  # Can be omitted
            server.login(self.smtp_username, self.password)
            server.sendmail(self.sender_email, receiver_email, message)
        except Exception as e:
            # Print any error messages to stdout
            self.log.error(str(e))
        finally:
            server.quit()
            return
