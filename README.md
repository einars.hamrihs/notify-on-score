# notify-on-score
Periodically checks scores on live score websites and notifies when a certain score is found.

## Getting started
Put configuration in .env file
> twistd -ny $SERVICE.tac

### Pre-requisites

Install Google chrome
> sudo curl -sS -o - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add
> sudo bash -c "echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' >> /etc/apt/sources.list.d/google-chrome.list"
> sudo apt -y update
> sudo apt -y install google-chrome-stable

Create a python virtual environment and install the requirements with following commands in project directory.
> python3.10 -m venv venv\
> source venv/bin/activate\
> pip install -r requirements.txt

## pre-commit

To make sure, there are no code or formatting errors, before every commit, run:
> pre-commit run --all-files
